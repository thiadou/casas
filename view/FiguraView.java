package objetosEnClase.view;

import java.awt.EventQueue;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import objetosEnClase.controller.FiguraController;
import objetosEnClase.modelo.Circulo;
import objetosEnClase.modelo.Cuadrado;
import objetosEnClase.modelo.Figura;
import objetosEnClase.modelo.Rectangulo;
import objetosEnClase.modelo.TrianguloRectangulo;
import objetosEnClase.modelo.exception.FiguraException;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import java.awt.Color;

public class FiguraView {
	private JFrame frame;
	private JTextField textValor;
	private JTable tablFiguras;
	private List<Figura> figuras;
	private String arrayfiguras[][];
	private Figura figuraAmodificarEliminar;
	private JTextField textNombre;
	private JTextField textBase;
	private JTextField textField_2;
	private JTextField textAltura;
	private JTextField textField_4;
	private JTextField textField_5;
	private JLabel lblSuperficieMaxima;
	private FiguraController figuraController = new FiguraController();
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FiguraView window = new FiguraView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public FiguraView() {
		initialize();
	}
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.BLACK);
		frame.setBackground(Color.BLACK);
		frame.setBounds(10, 10, 1024, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		JLabel lblNewLabel = new JLabel("Figuras Geometricas");
		lblNewLabel.setBackground(Color.BLUE);
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setFont(new Font("ISOCPEUR", Font.BOLD | Font.ITALIC, 24));
		lblNewLabel.setBounds(345, 27, 262, 29);
		frame.getContentPane().add(lblNewLabel);
		JPanel panel = new JPanel();
		panel.setBackground(Color.CYAN);
		panel.setBorder(new TitledBorder(null, "Una variable", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 136, 462, 106);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		JLabel lblValor = new JLabel("Lado/Radio");
		lblValor.setForeground(Color.BLUE);
		lblValor.setBounds(234, 46, 99, 20);
		panel.add(lblValor);
		lblValor.setFont(new Font("Times New Roman", Font.BOLD, 16));
		textValor = new JTextField();
		textValor.setBounds(340, 45, 112, 20);
		panel.add(textValor);
		textValor.setFont(new Font("Tahoma", Font.BOLD, 16));
		textValor.setColumns(10);
		JButton btnCrearCuadrado = new JButton("Crear Cuadrado");
		btnCrearCuadrado.setBackground(Color.RED);
		btnCrearCuadrado.setForeground(Color.BLUE);
		btnCrearCuadrado.setBounds(10, 24, 176, 29);
		panel.add(btnCrearCuadrado);
		btnCrearCuadrado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cuadrado cua =new Cuadrado(textNombre.getText(), Float.parseFloat(textValor.getText()));
				try {
					figuraController.addHandler(cua);
					figuras.add(cua);
					llenarGrilla(figuras);
					limpiarCampos();
				} catch (FiguraException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
					e1.printStackTrace();
				}
					}
		});
		btnCrearCuadrado.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD | Font.ITALIC, 16));
		JButton btnCrearCirculo = new JButton("Crear Circulo");
		btnCrearCirculo.setBackground(Color.BLACK);
		btnCrearCirculo.setForeground(Color.RED);
		btnCrearCirculo.setBounds(10, 64, 176, 29);
		panel.add(btnCrearCirculo);
		btnCrearCirculo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Circulo cir = 	new Circulo(textNombre.getText(), Float.parseFloat(textValor.getText()));
				try {
					figuraController.addHandler(cir);
					figuras.add(cir);
				} catch (FiguraException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
					e1.printStackTrace();
				}
				llenarGrilla(figuras);
				limpiarCampos();
			}
		});
		btnCrearCirculo.setFont(new Font("Kristen ITC", Font.BOLD | Font.ITALIC, 16));
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setForeground(Color.RED);
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(figuraAmodificarEliminar instanceof Cuadrado){
					Cuadrado cuad = (Cuadrado) figuraAmodificarEliminar;
					cuad.setNombre(textNombre.getText());
					cuad.setLado(Float.parseFloat(textValor.getText()));
				}
				else if(figuraAmodificarEliminar instanceof Circulo){
					Circulo cir = (Circulo) figuraAmodificarEliminar;
					cir.setNombre(textNombre.getText());
					cir.setRadio(Float.parseFloat(textValor.getText()));
				}
				else if(figuraAmodificarEliminar instanceof Rectangulo){
					Rectangulo rec = (Rectangulo) figuraAmodificarEliminar;
					rec.setBase(Float.parseFloat(textBase.getText()));
					rec.setAltura(Float.parseFloat(textAltura.getText()));
				}
				llenarGrilla(figuras);
				figuraAmodificarEliminar = null;
				limpiarCampos();
			}
		});
		btnModificar.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		btnModificar.setBounds(406, 603, 176, 30);
		frame.getContentPane().add(btnModificar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setForeground(Color.DARK_GRAY);
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				figuras.remove(figuraAmodificarEliminar);
				llenarGrilla(figuras);
				limpiarCampos();
				figuraAmodificarEliminar=null;
			}
		});
		btnEliminar.setFont(new Font("Britannic Bold", Font.BOLD | Font.ITALIC, 16));
		btnEliminar.setBounds(406, 631, 176, 31);
		frame.getContentPane().add(btnEliminar);
		
		JButton btnLimpiarCampos = new JButton("Limpiar campos");
		btnLimpiarCampos.setForeground(Color.MAGENTA);
		btnLimpiarCampos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCampos();
			}
		});
		btnLimpiarCampos.setFont(new Font("Modern No. 20", Font.BOLD | Font.ITALIC, 16));
		btnLimpiarCampos.setBounds(406, 658, 176, 32);
		frame.getContentPane().add(btnLimpiarCampos);
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setForeground(Color.ORANGE);
		lblNombre.setFont(new Font("Wide Latin", Font.BOLD, 16));
		lblNombre.setBounds(170, 79, 127, 20);
		frame.getContentPane().add(lblNombre);
		textNombre = new JTextField();
		textNombre.setFont(new Font("Tahoma", Font.BOLD, 16));
		textNombre.setColumns(10);
		textNombre.setBounds(170, 99, 112, 20);
		frame.getContentPane().add(textNombre);
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.RED);
		panel_1.setLayout(null);
		panel_1.setBorder(new TitledBorder(null, "Dos variables", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(521, 136, 462, 106);
		frame.getContentPane().add(panel_1);
		JLabel lblBase = new JLabel("Base");
		lblBase.setForeground(Color.GREEN);
		lblBase.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblBase.setBounds(10, 65, 60, 20);
		panel_1.add(lblBase);
		textBase = new JTextField();
		textBase.setFont(new Font("Tahoma", Font.BOLD, 16));
		textBase.setColumns(10);
		textBase.setBounds(75, 65, 112, 20);
		panel_1.add(textBase);
		JButton btnCrearTriangulo = new JButton("Crear Triangulo");
		btnCrearTriangulo.setBackground(Color.ORANGE);
		btnCrearTriangulo.setForeground(Color.MAGENTA);
		btnCrearTriangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				figuras.add(new TrianguloRectangulo(textNombre.getText(),
							Float.parseFloat(textBase.getText()), 
							Float.parseFloat(textAltura.getText())));
				llenarGrilla(figuras);
				limpiarCampos();
			}
		});
		btnCrearTriangulo.setFont(new Font("Swis721 Lt BT", Font.BOLD | Font.ITALIC, 16));
		btnCrearTriangulo.setBounds(237, 61, 176, 29);
		panel_1.add(btnCrearTriangulo);
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setForeground(Color.RED);
		lblAltura.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAltura.setBounds(237, 30, 60, 20);
		panel_1.add(lblAltura);
		textAltura = new JTextField();
		textAltura.setFont(new Font("Tahoma", Font.BOLD, 16));
		textAltura.setColumns(10);
		textAltura.setBounds(307, 30, 112, 20);
		panel_1.add(textAltura);
		JButton btnCrearRectangulo = new JButton("Crear Rectangulo");
		btnCrearRectangulo.setBackground(Color.GREEN);
		btnCrearRectangulo.setBounds(10, 25, 176, 29);
		panel_1.add(btnCrearRectangulo);
		btnCrearRectangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				figuras.add(new Rectangulo(	textNombre.getText(), 
											Float.parseFloat(textBase.getText()), 
											Float.parseFloat(textAltura.getText())));
				llenarGrilla(figuras);
				limpiarCampos();
			}
		});
		btnCrearRectangulo.setFont(new Font("Yu Gothic UI Semilight", Font.BOLD | Font.ITALIC, 16));
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.LIGHT_GRAY);
		panel_2.setLayout(null);
		panel_2.setBorder(new TitledBorder(null, "Tres variables", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(245, 250, 573, 158);
		frame.getContentPane().add(panel_2);
		JLabel lblValor_2 = new JLabel("Lado");
		lblValor_2.setBackground(Color.ORANGE);
		lblValor_2.setForeground(Color.GREEN);
		lblValor_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblValor_2.setBounds(114, 30, 54, 20);
		panel_2.add(lblValor_2);
		textField_2 = new JTextField();
		textField_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		textField_2.setColumns(10);
		textField_2.setBounds(82, 61, 112, 20);
		panel_2.add(textField_2);
		JButton btnCrearCuadrado_2 = new JButton("Crear Poligono");
		btnCrearCuadrado_2.setBackground(Color.RED);
		btnCrearCuadrado_2.setForeground(Color.ORANGE);
		btnCrearCuadrado_2.setFont(new Font("Agency FB", Font.BOLD | Font.ITALIC, 16));
		btnCrearCuadrado_2.setBounds(53, 106, 176, 29);
		panel_2.add(btnCrearCuadrado_2);
		JLabel lblValor_2_1 = new JLabel("Cantidad de Lados");
		lblValor_2_1.setForeground(Color.BLACK);
		lblValor_2_1.setFont(new Font("Century Gothic", Font.BOLD, 16));
		lblValor_2_1.setBounds(361, 92, 176, 20);
		panel_2.add(lblValor_2_1);
		textField_4 = new JTextField();
		textField_4.setFont(new Font("Tahoma", Font.BOLD, 16));
		textField_4.setColumns(10);
		textField_4.setBounds(377, 110, 112, 20);
		panel_2.add(textField_4);
		JLabel lblValor_2_2 = new JLabel("Apotema");
		lblValor_2_2.setBackground(Color.BLUE);
		lblValor_2_2.setForeground(Color.CYAN);
		lblValor_2_2.setFont(new Font("Baskerville Old Face", Font.BOLD, 16));
		lblValor_2_2.setBounds(402, 41, 74, 20);
		panel_2.add(lblValor_2_2);
		textField_5 = new JTextField();
		textField_5.setFont(new Font("Tahoma", Font.BOLD, 16));
		textField_5.setColumns(10);
		textField_5.setBounds(377, 61, 112, 20);
		panel_2.add(textField_5);
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setBounds(270, 419, 573, 173);
				frame.getContentPane().add(scrollPane);
				tablFiguras = new JTable();
				tablFiguras.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						figuraAmodificarEliminar= figuras.get(tablFiguras.getSelectedRow());
						if(figuraAmodificarEliminar instanceof Cuadrado)
							asignarValores((Cuadrado)figuraAmodificarEliminar);
						else if(figuraAmodificarEliminar instanceof Circulo)
							asignarValores((Circulo)figuraAmodificarEliminar);
						else if(figuraAmodificarEliminar instanceof Rectangulo)
							asignarValores((Rectangulo)figuraAmodificarEliminar);
						else if(figuraAmodificarEliminar instanceof TrianguloRectangulo)	
							asignarValores((TrianguloRectangulo)figuraAmodificarEliminar);
					}
				});
				tablFiguras.setFont(new Font("Tahoma", Font.BOLD, 16));
						scrollPane.setViewportView(tablFiguras);
						JLabel lblNewLabel_1 = new JLabel("Maxima superficie");
						lblNewLabel_1.setForeground(Color.MAGENTA);
						lblNewLabel_1.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 24));
						lblNewLabel_1.setBounds(601, 67, 242, 29);
						frame.getContentPane().add(lblNewLabel_1);
						lblSuperficieMaxima = new JLabel("");
						lblSuperficieMaxima.setForeground(Color.RED);
						lblSuperficieMaxima.setBackground(Color.ORANGE);
						lblSuperficieMaxima.setOpaque(true);
						lblSuperficieMaxima.setFont(new Font("Tahoma", Font.BOLD, 20));
						lblSuperficieMaxima.setBounds(637, 95, 141, 25);
						frame.getContentPane().add(lblSuperficieMaxima);
		asignarValoresIniciales();
	}
	private void asignarValores(Cuadrado pCua){
		textNombre.setText(pCua.getNombre());
		textValor.setText(Float.toString(pCua.getLado()));
	}
	private void asignarValores(Circulo pCir){
		textNombre.setText(pCir.getNombre());
		textValor.setText(Float.toString(pCir.getRadio()));
	}
	private void asignarValores(Rectangulo pRec){
		textNombre.setText(pRec.getNombre());
		textBase.setText(Float.toString(pRec.getBase()));
		textAltura.setText(Float.toString(pRec.getAltura()));
	}
	private void asignarValores(TrianguloRectangulo pTri){
		textNombre.setText(pTri.getNombre());
		textBase.setText(Float.toString(pTri.getBase()));
		textAltura.setText(Float.toString(pTri.getAltura()));
	}
	private void llenarGrilla(List<Figura> pFiguras){
		int fila =0 ;
		arrayfiguras = new String[pFiguras.size()][4];
		//esto es para formatear a dos decimales
		DecimalFormat df = new DecimalFormat("#.##");
		for (Figura figura : pFiguras) {
			for(int col=0;col<4;col++){
				switch (col) {
				case 0:
					arrayfiguras[fila][col] = figura.getNombre();					
					break;
				case 1:
					arrayfiguras[fila][col] = figura.getValores();					
					break;					
				case 2:
					arrayfiguras[fila][col] = df.format(figura.calcularPerimetro());					
					break;
				case 3:
					arrayfiguras[fila][col] = df.format(figura.calcularSuperficie());					
					break;
				default:
					break;
				}				
			}
			fila++;
		}
		lblSuperficieMaxima.setText(df.format(Figura.getMaximaSuperficie()));
		tablFiguras.setModel(new DefaultTableModel(
				arrayfiguras,
				new String[] {
					"Nombre","valores", "perimetro", "superficie"
				}
			));
	}
	private void asignarValoresIniciales(){
		figuras= new ArrayList<Figura>();
		figuras.add(new Cuadrado("esfera cuadrada 1", 15));
		figuras.add(new Circulo("cuadrado circular 1", 10));
		figuras.add(new Cuadrado("esfera cuadrada 2", 20));
		figuras.add(new Circulo("cuadrado circular 2", 30));
		llenarGrilla(figuras);
	}
	private void limpiarCampos() {
		textNombre.setText("");
		textValor.setText("");
		textBase.setText("");
		textAltura.setText("");
	}
}